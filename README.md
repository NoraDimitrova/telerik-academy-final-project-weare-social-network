# Final Project QA Telerik - Social Network
![Team Project Assignment](telerik.PNG)


## I. INTRODUCTION

[WEare Social Network](https://shielded-ridge-13380.herokuapp.com/) is a web application that is written in Java and allows you to connect and communicate with other people. The application has a public part (for anonymous users), a private part (for registered users) and an administrative part (for system administrators). It allows you to:

- Connect with other by sending connection requests;
- Manage your personal profile information;
- Creating posts, commenting and liking posts;
- Get a feed of posts from people in your network.

Functional requirements for the application can be found here: [Telerik Academy Project Social Network](https://drive.google.com/file/d/1Qn-OY4Gv_zXAei1Q8mwL09pWwzcebs5s/view)

## II. QA TEAM
- Dimitrinka Dimitrova
- Nora Dimitrova
- Anna Spassova

## III. DELIVERABLES

1. [Test plan v 1.3](https://drive.google.com/file/d/1q5eShNmWSYIcxD_2FmUcIMxqDUNzleZn/view?usp=sharing)
2. [Test cases Jira/XRay](https://noradimitrova.atlassian.net/jira/software/projects/FPA3QSN/boards/5)
3. [Test case template](https://drive.google.com/file/d/1spdhMBAOsDe3nYU9AJ9fjQ4PKxke4TNV/view?usp=sharing)
4. [Bug report template](https://drive.google.com/file/d/19KZ7l5Tiz58GgZMeXsogo3hBHW3eD-He/view?usp=sharing)
5. [Exploratory testing document](https://drive.google.com/file/d/1kt6FYm0IJDUoVyL2o4urQ6p7M4Mu0k0u/view?usp=sharing)
6. [Jira Test cases document](https://drive.google.com/file/d/1A2cQdKA0m5267UKOsbssBfe6ltm4dDTa/view?usp=sharing)
7. [API Automation tests in Postman](https://gitlab.com/final-project-qa-telerik/final-project-qa-telerik-social-network/-/tree/main/Postman)
8. [UI Selenium tests / Java](https://gitlab.com/final-project-qa-telerik/final-project-qa-telerik-social-network/-/tree/main/Selenium/Social%20Network)
9. [API Postman Report](https://drive.google.com/file/d/1Yz3VnMjttHWN6btNTsjEEGfs9FhL4fdS/view?usp=sharing)
10. [UI Selenium Report](https://drive.google.com/file/d/1Xv3KDVBL_0V-SdLXjkEk03XPH1Rjw7nN/view?usp=sharing)
11. [Test Summary Report](https://drive.google.com/file/d/1COtsi7sx8MxZ_P6iENwgaVjWmHnuhT3C/view?usp=sharing)
13. [Website WEare Social Network](https://shielded-ridge-13380.herokuapp.com/)


## IV. HOW TO RUN AUTOMATIONS

###Running the automations

Run Postman API tests

1. [Clone the repo on your computer](https://gitlab.com/final-project-qa-telerik/final-project-qa-telerik-social-network.git)
2. Go to the Postman folder on your device and double-click on the SocialNetworkPostmanRun.bat file.

Run Selenium UI tests
1. [Clone the repo on your computer](https://gitlab.com/final-project-qa-telerik/final-project-qa-telerik-social-network.git)
2. Go to the Selenium/Social Network folder on your device and double-click on the SocialNetworkSeleniumRun.bat file.


