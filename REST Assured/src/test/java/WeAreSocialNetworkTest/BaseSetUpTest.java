package WeAreSocialNetworkTest;

import io.restassured.RestAssured;
import io.restassured.http.Cookies;
import org.testng.annotations.BeforeTest;

import static com.api.tests.Constants.BASE_URL;
import static com.api.tests.Endpoints.LOGIN_USER;
import static io.restassured.RestAssured.baseURI;
import static java.lang.String.format;

public class BaseSetUpTest {
    public static Cookies cookies;


    @BeforeTest
    public void takeCookiesTests() {
        baseURI = format("%s%s", BASE_URL, LOGIN_USER);

        cookies = RestAssured.given()
                .when()
                .post(baseURI)
                .then()
                .statusCode(302)
                .extract()
                .response()
                .getDetailedCookies();

        System.out.println(cookies);

    }

}
