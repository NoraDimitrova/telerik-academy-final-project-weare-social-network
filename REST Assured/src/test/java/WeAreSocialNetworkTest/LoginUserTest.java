package WeAreSocialNetworkTest;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.api.tests.Constants.*;
import static com.api.tests.Endpoints.LOGIN_USER;
import static com.api.tests.Helper.isValid;
import static io.restassured.RestAssured.baseURI;
import static java.lang.String.format;


public class LoginUserTest extends BaseSetUpTest {


    @Test(priority = 3)
    public void loginTest() {

        baseURI = format("%s%s", BASE_URL, LOGIN_USER);
        System.out.println(baseURI);

        Response response = RestAssured.given()
                .cookies(cookies)
                .queryParam("username", USERNAME)
                .queryParam("password", PASSWORD)
                .header("Content-Type", "application/json")
                .log().all().contentType("text/htm; charset=UTF-8")
                .post(baseURI);

        System.out.println(response.getStatusCode());
        Assert.assertEquals(response.getStatusCode(), 302, "Incorrect status code. Expected 302.");


        authCookie = cookies.getValue("JSESSIONID");
        System.out.println(authCookie);
        isValid(authCookie);

    }

}




