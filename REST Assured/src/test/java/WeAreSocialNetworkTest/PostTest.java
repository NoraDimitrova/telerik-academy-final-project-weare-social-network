package WeAreSocialNetworkTest;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.api.tests.Constants.*;
import static com.api.tests.Endpoints.*;
import static com.api.tests.Helper.isValid;
import static com.api.tests.RequestJSON.*;
import static io.restassured.RestAssured.baseURI;
import static java.lang.String.format;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class PostTest extends BaseSetUpTest {

    public static int postId;

    @Test(priority = 4)
    public void addNewPostTest() {

        baseURI = format("%s%s", BASE_URL, CREATE_POST);
        System.out.println(baseURI);

        String requestBody = (format(POST_BODY, POST_MESSAGE));
        assertTrue(isValid(requestBody), "Body is not a valid JSON");
        System.out.println(requestBody);

        Response response = RestAssured.given()
                .cookies(cookies)
                .contentType("application/json")
                .body(requestBody)
                .when()
                .post(baseURI);

        response.print();

        int statusCode = response.getStatusCode();
        assertEquals(statusCode, 200, "Incorrect status code. Expected 200.");

        Assert.assertTrue((response.getBody().jsonPath().get("content")).equals(POST_MESSAGE));

        postId = response.then().extract().path("postId");
        System.out.println(postId);


    }


    @Test(priority = 5)
    public void getUserPostsByUserIdTest() {

        baseURI = format("%s%s", BASE_URL, GET_POST);
        System.out.println(baseURI);

        String requestBody = (format(GET_ALL_POST_BODY));
        assertTrue(isValid(requestBody), "Body is not a valid JSON");

        Response response = RestAssured.given()
                .cookies(cookies)
                .contentType("application/json")
                .body(requestBody)
                .when()
                .get(baseURI);


        int statusCode = response.getStatusCode();
        assertEquals(statusCode, 200, "Incorrect status code. Expected 200.");

        System.out.println(response.getBody().jsonPath().prettify());

    }

    @Test(priority = 6)
    public void editPostByPostIdTest() {

        baseURI = format("%s%s", BASE_URL, EDIT_POST);
        System.out.println(baseURI);

        String requestBody = (format(EDIT_POST_BODY, EDIT_POST_MESSAGE));
        assertTrue(isValid(requestBody), "Body is not a valid JSON");

        Response response = RestAssured.given()
                .cookies(cookies)
                .contentType("application/json")
                .body(requestBody)
                .when()
                .put(baseURI);

        int statusCode = response.getStatusCode();
        System.out.println("Status code is: " + statusCode);

        assertEquals(statusCode, 200, "Incorrect status code. Expected 200.");

        System.out.println("Response body is empty" + response.getBody().prettyPrint());
        assertEquals(response.body().asString(), "", "Response body is not empty");

    }

    @Test(priority = 7)
    public void likeAndDislikePostTest() {

        baseURI = format("%s%s", BASE_URL, LIKE_POST);
        System.out.println(baseURI);

        Response response = RestAssured.given()
                .cookies(cookies)
                .contentType("application/json")
                .when()
                .post(baseURI);

        int statusCode = response.getStatusCode();
        System.out.println(statusCode);
        Assert.assertEquals(statusCode, 200, "Incorrect status code. Expected 200.");

        Boolean liked = response.then().extract().path("liked");
        System.out.println(liked);

        if (liked.equals(true)) {
            System.out.println("Post is liked");
        } else {
            System.out.println("Post is dislike");
        }

        System.out.println(response.getBody().prettyPrint());

    }

    @Test(priority = 8)
    public void deletePostTest(){

        baseURI=format("%s%s", BASE_URL, DELETE_POST);
        System.out.println(baseURI);

        Response response = RestAssured.given()
                .cookies(cookies)
                .contentType("application/json")
                .when()
                .delete(baseURI);

        int statusCode = response.getStatusCode();
        System.out.println(statusCode);
        Assert.assertEquals(statusCode, 200, "Incorrect status code. Expected 200.");


    }

}
