package WeAreSocialNetworkTest;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import static com.api.tests.Constants.*;
import static com.api.tests.Endpoints.GET_REGISTER_USERS;
import static com.api.tests.Endpoints.REGISTER_USER;
import static com.api.tests.Helper.isValid;
import static com.api.tests.RequestJSON.GET_ALL_REGISTER_USERS_BODY;
import static com.api.tests.RequestJSON.REGISTER_USER_BODY;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static java.lang.String.format;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class RegisterUserTest {


    @Test(priority = 1)
    public void registerNewUserTest() {

        baseURI = format("%s%s", BASE_URL, REGISTER_USER);
        System.out.println(baseURI);

        String requestBody = (format(REGISTER_USER_BODY, PASSWORD, EMAIL, PASSWORD, USERNAME));
        assertTrue(isValid(requestBody), "Body is not a valid JSON");
        System.out.println(requestBody);

        Response response = given()
                .contentType("application/json")
                .body(requestBody)
                .when()
                .post();

        response.print();

        int statusCode = response.getStatusCode();
        assertEquals(statusCode, 200, "Incorrect status code. Expected 200.");

    }


    @Test(priority = 2)
    public void getAllRegisterUsersTest() {

        baseURI = format("%s%s", BASE_URL, GET_REGISTER_USERS);
        System.out.println(baseURI);

        String requestBody = (format(GET_ALL_REGISTER_USERS_BODY));
        assertTrue(isValid(requestBody), "Body is not a valid JSON");
        System.out.println(requestBody);

        Response response = RestAssured.given()
                .contentType("application/json")
                .body(requestBody)
                .when()
                .post();

        response.print();

        String userId = String.valueOf(response.getBody().jsonPath().getInt("userId[0]"));
        System.out.println(userId);

        int statusCode = response.getStatusCode();
        assertEquals(statusCode, 200, "Incorrect status code. Expected 200.");

        System.out.println(response.getBody().jsonPath().prettify());

    }

}
