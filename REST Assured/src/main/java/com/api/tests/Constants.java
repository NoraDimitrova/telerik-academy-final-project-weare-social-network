package com.api.tests;


public class Constants {

    public static final String EMAIL = "ana_maria@abv.bg";
    public static final String USERNAME = "Ana";
    public static final String PASSWORD = "Password18AM";
    public static final String BASE_URL = "https://shielded-ridge-13380.herokuapp.com";
    public static String authCookie;
    public static final String POST_MESSAGE = "I am the new intern";
    public static String userId = "405"; // put here userId from test 2 of RegisterUserTest class
    public static final String EDIT_POST_MESSAGE = "I am a girl";
    public static final String POST_ID = "189"; // put here userId from test 5 of PostTest class


}
