package com.api.tests;

import static com.api.tests.Constants.*;

public class Endpoints {

    public static final String REGISTER_USER = "/api/users/";
    public static final String GET_REGISTER_USERS = "/api/users";
    public static final String LOGIN_USER = "/authenticate?username=" + USERNAME + "&password=" + PASSWORD;
    public static final String CREATE_POST = "/api/post/auth/creator?username=" + USERNAME;
    public static final String GET_POST = "/api/users/" + userId + "/posts";
    public static final String EDIT_POST = "/api/post/auth/editor/?username=" + USERNAME + "&postId=" + POST_ID;
    public static final String LIKE_POST = "/api/post/auth/likesUp?username=" + USERNAME + "&postId=" + POST_ID;
    public static final String DELETE_POST = "/api/post/auth/manager?username=" + USERNAME + "&postId=" + POST_ID;


}
