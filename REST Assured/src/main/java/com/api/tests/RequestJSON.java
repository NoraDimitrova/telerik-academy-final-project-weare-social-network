package com.api.tests;

public class RequestJSON {

    public static final String REGISTER_USER_BODY = "{  \n" +
            "    \"authorities\": [\n" +
            "    \"ROLE_USER\"\n" +
            "  ],\n" +
            "    \"category\": {\n" +
            "    \"id\": 119,\n" +
            "    \"name\": \"Gardener\"\n" +
            "  },    \n" +
            "  \"confirmPassword\": \"%s\",\n" +
            "  \"email\": \"%s\",\n" +
            "  \"password\": \"%s\",\n" +
            "  \"username\": \"%s\"\n" +
            "}";


    public static final String GET_ALL_REGISTER_USERS_BODY = "{\n" +
            "  \"index\": 0,\n" +
            "  \"next\": true,\n" +
            "  \"searchParam1\": \"\",\n" +
            "  \"searchParam2\": \"\",\n" +
            "  \"size\": 15\n" +
            "}";


    public static final String POST_BODY = "{\n" +
            "  \"content\": \"%s\",\n" +
            "  \"picture\": \"\",\n" +
            "  \"public\": true\n" +
            "}";


    public static final String GET_ALL_POST_BODY = "{\n" +
            "  \"index\": 0,\n" +
            "  \"next\": true,\n" +
            "  \"searchParam1\": \"\",\n" +
            "  \"searchParam2\": \"\",\n" +
            "  \"size\": 10\n" +
            "}";


    public static final String EDIT_POST_BODY = "{\n" +
            "  \"content\": \"%s\",\n" +
            "  \"picture\": \"\",\n" +
            "  \"public\": true\n" +
            "}";


}
