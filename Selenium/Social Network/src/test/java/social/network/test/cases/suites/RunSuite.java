package social.network.test.cases.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import social.network.test.cases.ProfileManagementTest;
import social.network.test.cases.RegistrationTest;
import social.network.test.cases.TopicTest;

@Suite.SuiteClasses({RegistrationTest.class, ProfileManagementTest.class,TopicTest.class})
@RunWith(Suite.class)
public class RunSuite {
}

