package social.network.test.cases;

import jdk.jfr.Description;
import org.junit.Test;
import pages.telerikacademy.social.network.TopicPage;

public class TopicTest extends BaseTestSetup {

    TopicPage topicPage = new TopicPage(actions.getDriver());

    @Test
    @Description("FPA3QSN-128 User is able to create post successfully - Happy Path")
    public void addNewPostTest() {
        registrationAndLogin();
        topicPage.addNewPost();
        logOut();
    }

    @Test
    @Description("FPA3QSN-76 User is unable to create post with 1001 characters in message box.")
    public void addNewPostWithMoreSymbolsTest() {
        registrationAndLogin();
        topicPage.addNewPostWithMoreSymbols();
        logOut();
    }

    @Test
    @Description("FPA3QSN-107 Test User is able to like post from other users /" +
            " FPA3QSN-108 Test User is able to unlike the post if he has already liked it")
    public void likeDislikePostTest() {
        addNewPostWithMoreSymbolsTest();
        registrationAndLogin();
        topicPage.likePost();
        topicPage.dislikePost();
        logOut();
    }

}
