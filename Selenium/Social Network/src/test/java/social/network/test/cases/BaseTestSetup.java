package social.network.test.cases;

import com.telerikacademy.testframework.UserActions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import pages.telerikacademy.social.network.LoginPage;
import pages.telerikacademy.social.network.RegistrationPage;

public class BaseTestSetup {

    UserActions actions = new UserActions();

    @BeforeClass
    public static void setUp() {
        UserActions.loadBrowser("home.page");
    }


    @AfterClass
    public static void tearDown() {
        UserActions.quitDriver();
    }

    LoginPage loginPage = new LoginPage(actions.getDriver());
    RegistrationPage registration = new RegistrationPage(actions.getDriver());


    public void registrationAndLogin() {
        registration.registrationWithValidFields();
        loginPage.loginUser();
    }

    public void logOut() {
        loginPage.logOut();
    }
}
