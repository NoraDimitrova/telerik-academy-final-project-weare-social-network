package social.network.test.cases;

import jdk.jfr.Description;
import org.junit.Test;
import pages.telerikacademy.social.network.ProfileManagementPage;

public class ProfileManagementTest extends BaseTestSetup {

    ProfileManagementPage profileManagementPage = new ProfileManagementPage(actions.getDriver());

    @Test
    @Description("FPA3QSN-182 User is able to set first name, last name and date of birth in his profile page.")
    public void setProfileDataTest() {
        registrationAndLogin();
        profileManagementPage.setProfileData();
        logOut();
    }

    @Test
    @Description("FPA3QSN-64 Test User is able to change birthday date")
    public void updateBirthdayTest() {
        registrationAndLogin();
        profileManagementPage.updateBirthday();
        logOut();
    }

    @Test
    @Description("FPA3QSN-55 User is able to change their email")
    public void updateEmailTest() {
        registrationAndLogin();
        profileManagementPage.updateEmail();
        logOut();
    }

    @Test
    @Description("FPA3QSN-61 User is able to change field Drop few words about yourself")
    public void dropFewWordsAboutYourselfTest() {
        registrationAndLogin();
        profileManagementPage.dropFewWordsAboutYourself();
        logOut();
    }

    @Test
    @Description("FPA3QSN-18 It is impossible for an user not to fill in a first name field")
    public void firstNameFieldBlankTest() {
        registrationAndLogin();
        profileManagementPage.firstNameFieldBlank();
        logOut();
    }

}


