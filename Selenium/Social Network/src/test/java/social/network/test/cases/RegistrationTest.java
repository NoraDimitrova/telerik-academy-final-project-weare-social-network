package social.network.test.cases;

import jdk.jfr.Description;
import org.junit.Test;
import pages.telerikacademy.social.network.RegistrationPage;

public class RegistrationTest extends BaseTestSetup {

    RegistrationPage registrationPage = new RegistrationPage(actions.getDriver());

    @Test()
    @Description("FPA3QSN-38 User is able to register by filling in all the required fields with valid information")
    public void registrationOfNewUserTest() {
        registrationPage.registrationWithValidFields();
    }

    @Test
    @Description("FPA3QSN-47 Test User is unable to register by filling in invalid email")
    public void registrationWithInvalidEmailTest() {
        registrationPage.registrationWithInvalidEmail();
    }

    @Test
    @Description("FPA3QSN-39 Test User is unable to register by leaving all required fields blank")
    public void registrationByEmptyFieldsTest() {
        registrationPage.registrationByEmptyRequiredFields();
    }

    @Test
    @Description("FPA3QSN-43 User is unable to register by leaving confirm password field blank")
    public void registrationByEmptyConfirmPasswordTest() {
        registrationPage.registrationByEmptyConfirmPassword();
    }

    @Test
    @Description("FPA3QSN-172 User is unable to register by filling in already registered username")
    public void registrationWithRegisteredUsernameTest() {
        registrationPage.registrationWithAlreadyRegisteredUsername();
    }
}
