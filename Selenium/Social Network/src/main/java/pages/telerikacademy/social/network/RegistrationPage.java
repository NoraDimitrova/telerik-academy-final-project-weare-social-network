package pages.telerikacademy.social.network;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebDriver;

import static pages.telerikacademy.social.network.utils.Constants.*;

public class RegistrationPage extends BaseSocialNetworkPage {

    public RegistrationPage(WebDriver driver) {
        super(driver, "home.page");
    }

    public void registrationWithValidFields() {
        navigateToPage();
        randomUsername = RandomStringUtils.random(10, true, false);
        username = randomUsername;
        registration(username, EMAIL, PASSWORD, PASSWORD);
        actions.waitForElementVisible("register.update.profile");
        actions.assertElementPresent("register.update.profile");
    }

    public void registrationByEmptyRequiredFields() {
        registration(" ", " ", " ", " ");
        actions.assertElementNotPresent("register.update.profile");
    }

    public void registrationByEmptyConfirmPassword() {
        randomUsername = RandomStringUtils.random(10, true, false);
        username = randomUsername;
        registration(username, EMAIL, PASSWORD, " ");
        actions.assertElementPresent("register.error.message.confirm.password");
    }

    public void registrationWithInvalidEmail() {
        randomUsername = RandomStringUtils.random(10, true, false);
        username = randomUsername;
        registration(username, INVALID_EMAIL, PASSWORD, PASSWORD);
        actions.assertElementPresent("register.error.message.invalid.email");
    }

    public void registrationWithAlreadyRegisteredUsername() {
        registration(username, EMAIL, PASSWORD, PASSWORD);
        registration(username, EMAIL, PASSWORD, PASSWORD);
        actions.assertElementPresent("register.error.message.exist.username");
    }
}
