package pages.telerikacademy.social.network;

import org.openqa.selenium.WebDriver;
import static pages.telerikacademy.social.network.utils.Constants.*;

public class TopicPage extends BaseSocialNetworkPage {
    public TopicPage(WebDriver driver) {
        super(driver, "home.page");
    }


    public void addNewPost() {
        actions.waitAndClick("topic.button.add.new.post");
        actions.waitAndClick("topic.button.post.visibility");
        actions.waitAndClick("topic.button.public.post.visibility");
        actions.waitAndClick("topic.message.field");
        actions.typeValueInField(MESSAGE_POST, "topic.message.field");
        actions.clickElement("topic.button.save.post");
        actions.assertElementPresent("topic.assert.post");
        navigateToPage();
    }

    public void addNewPostWithMoreSymbols() {
        actions.waitAndClick("topic.button.add.new.post");
        actions.waitAndClick("topic.button.post.visibility");
        actions.waitAndClick("topic.button.public.post.visibility");
        actions.waitAndClick("topic.message.field");
        actions.typeValueInField(POST_DATA_WITH_MORE_SYMBOLS, "topic.message.field");
        actions.clickElement("topic.button.save.post");
        actions.assertElementPresent("topic.error.message");
        navigateToPage();
    }

    public void likePost() {
        actions.waitAndClick("topic.latest.post");
        actions.waitAndClick("topic.like.button");
        actions.waitAndClick("topic.latest.post");
        actions.waitForElementVisible("topic.dislike.button");
        actions.assertElementPresent("topic.dislike.button");
        navigateToPage();
    }

    public void dislikePost() {
        actions.waitAndClick("topic.latest.post");
        actions.waitAndClick("topic.dislike.button");
        actions.waitAndClick("topic.latest.post");
        actions.waitForElementPresent("topic.like.button");
        actions.assertElementPresent("topic.like.button");
        navigateToPage();
    }


}
