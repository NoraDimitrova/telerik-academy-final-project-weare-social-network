package pages.telerikacademy.social.network;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import static pages.telerikacademy.social.network.utils.Constants.*;

public class ProfileManagementPage extends BaseSocialNetworkPage {

    public ProfileManagementPage(WebDriver driver) {
        super(driver, "home.page");
    }

    public void setProfileData() {
        actions.waitAndClick("profile.management.button.personal.profile");
        actions.waitAndClick("profile.management.button.edit.profile");
        setPersonalInformation(FIRSTNAME, LASTNAME, BIRTHDAY);
        actions.clickElement("profile.management.button.update.my.profile");
        actions.waitAndClick("profile.management.button.personal.profile");

        actions.waitForElementPresent("profile.management.firstNames.lastName");
        actions.assertElementPresent("profile.management.firstNames.lastName");
    }


    public void dropFewWordsAboutYourself() {
        actions.waitAndClick("profile.management.button.personal.profile");
        actions.waitAndClick("profile.management.button.edit.profile");
        setPersonalInformation(FIRSTNAME, LASTNAME, BIRTHDAY);
        actions.waitForElementClickable("profile.management.drop.few.words.field");
        actions.clickElement("profile.management.drop.few.words.field");
        actions.typeValueInField(FEW_WORDS, "profile.management.drop.few.words.field");
        actions.clickElement("profile.management.button.update.my.profile");
        actions.waitForElementClickable("profile.management.button.personal.profile");
        actions.clickElement("profile.management.button.personal.profile");

        actions.waitForElementPresent("profile.management.few.words.about.yourself");
        actions.assertElementPresent("profile.management.few.words.about.yourself");
    }

    public void updateBirthday() {
        actions.waitAndClick("profile.management.button.personal.profile");
        actions.waitAndClick("profile.management.button.edit.profile");
        setPersonalInformation(FIRSTNAME, LASTNAME, BIRTHDAY);
        actions.clickElement("profile.management.field.birthday");
        actions.pressKey(Keys.ENTER);
        actions.waitFor(2000);
        actions.typeValueInField(UPDATED_BIRTHDAY, "profile.management.field.birthday");
        actions.clickElement("profile.management.button.update.my.profile");

        actions.clickElement("profile.management.button.update.my.profile");
        actions.waitForElementClickable("profile.management.button.personal.profile");

        actions.clickElement("profile.management.button.personal.profile");
        actions.assertElementPresent("profile.management.edit.birthday.paragraph");
    }

    public void updateEmail() {
        actions.waitAndClick("profile.management.button.personal.profile");
        actions.waitAndClick("profile.management.button.edit.profile");
        setPersonalInformation(FIRSTNAME, LASTNAME, BIRTHDAY);
        actions.waitForElementPresent("profile.management.field.email");
        actions.clickElement("profile.management.field.email");
        actions.typeValueInField(UPDATED_EMAIL, "profile.management.field.email");
        actions.clickElement("profile.management.button.update.my.profile");
        actions.waitForElementClickable("profile.management.button.personal.profile");
        actions.clickElement("profile.management.button.personal.profile");
        actions.waitAndClick("profile.management.button.personal.profile");

        actions.assertElementIsVisible("profile.management.get.email", UPDATED_EMAIL);
    }

    public void firstNameFieldBlank() {

        actions.waitForElementClickable("profile.management.button.personal.profile");
        actions.clickElement("profile.management.button.personal.profile");
        actions.waitForElementPresent("profile.management.button.edit.profile");
        actions.clickElement("profile.management.button.edit.profile");
        setPersonalInformation("", LASTNAME, BIRTHDAY);
        actions.waitForElementClickable("profile.management.button.update.my.profile");
        actions.clickElement("profile.management.button.update.my.profile");
        actions.assertElementPresent("profile.management.first.name.message");
    }

}
