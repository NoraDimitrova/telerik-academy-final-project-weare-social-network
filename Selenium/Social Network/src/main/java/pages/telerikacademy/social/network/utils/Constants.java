package pages.telerikacademy.social.network.utils;

public class Constants {

    public static final String PASSWORD = "Password123";
    public static final String EMAIL = "test_user@abv.bg";
    public static final String FIRSTNAME = "Natalia";
    public static final String FIRSTNAME_BLANK = "";
    public static final String LASTNAME = "Petkova";
    public static final String BIRTHDAY = "10/11/2010";
    public static final String FEW_WORDS = "I love flowers!";
    public static final String UPDATED_EMAIL="updated_email@abv.bg";
    public static final String UPDATED_BIRTHDAY="5/10/2010";
    public static final String POST_DATA_WITH_MORE_SYMBOLS = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, " +
            "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nunc lobortis mattis aliquam faucibus " +
            "purus in massa tempor nec. Turpis in eu mi bibendum neque. Tortor pretium viverra suspendisse potenti " +
            "nullam. Ut etiam sit amet nisl purus in. Nascetur ridiculus mus mauris vitae. Facilisi cras fermentum " +
            "odio eu feugiat pretium nibh ipsum consequat. Pellentesque diam volutpat commodo sed egestas egestas " +
            "fringilla. Ut etiam sit amet nisl purus. Faucibus in ornare quam viverra orci sagittis eu volutpat. " +
            "Condimentum lacinia quis vel eros donec ac odio tempor orci. Quam quisque id diam vel quam elementum " +
            "pulvinar etiam non. Volutpat maecenas volutpat blandit aliquam etiam erat. Lorem ipsum dolor sit amet " +
            "consectetur adipiscing. Ut eu sem integer vitae justo eget magna fermentum iaculis. Luctus venenatis " +
            "lectus magna fringilla urna. Ornare lectus sit amet est commodo sed egestas commodo sed egestas commodo " +
            "sed egestas commodo seddd.";

    public static final String MESSAGE_POST = "Hello, what kind of flowers do you like?";
    public static final String INVALID_USERNAME = "ASuser";
    public static final String INVALID_EMAIL = "test_user47";
    public static String username ="Ali";
}
