package pages.telerikacademy.social.network;

import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import static pages.telerikacademy.social.network.utils.Constants.*;

public abstract class BaseSocialNetworkPage extends BasePage {
    public BaseSocialNetworkPage(WebDriver driver, String urlKey) {
        super(driver, urlKey);
    }

    String randomUsername;

    public void registration(String username, String email, String password, String confirm_password) {
        navigateToPage();
        actions.waitAndClick("register.button");
        actions.waitAndClick("register.name");
        actions.typeValueInField(username, "register.name");
        actions.clickElement("register.email");
        actions.typeValueInField(email, "register.email");
        actions.clickElement("register.password");
        actions.typeValueInField(password, "register.password");
        actions.clickElement("register.confirm.password");
        actions.typeValueInField(confirm_password, "register.confirm.password");
        actions.clickElement("register.factory.worker");
        actions.waitAndClick("register.button.register");
    }

    public void setPersonalInformation(String firstname, String lastname, String birthday) {
        actions.waitAndClick("profile.management.field.firstName");
        actions.typeValueInField(firstname, "profile.management.field.firstName");
        actions.clickElement("profile.management.field.lastName");
        actions.typeValueInField(lastname, "profile.management.field.lastName");
        actions.clickElement("profile.management.field.birthday");
        actions.pressKey(Keys.ENTER);
        actions.typeValueInField(birthday, "profile.management.field.birthday");
    }

}
