package pages.telerikacademy.social.network;

import org.openqa.selenium.WebDriver;

import static pages.telerikacademy.social.network.utils.Constants.*;

public class LoginPage extends BaseSocialNetworkPage {
    public LoginPage(WebDriver driver) {
        super(driver, "home.page");
    }

    public void loginUser() {
        navigateToPage();
        actions.clickElement("login.button");
        actions.waitForElementClickable("login.button");

        actions.typeValueInField(username, "login.username");
        actions.typeValueInField(PASSWORD, "login.password");
        actions.clickElement("login.button.signIn");

        actions.waitForElementVisible("home.page.user.logOut");
        actions.assertElementPresent("home.page.user.logOut");
    }

    public void logOut() {
        navigateToPage();
        actions.waitForElementClickable("home.page.user.logOut", 2000);
        actions.clickElement("home.page.user.logOut");
    }
}
