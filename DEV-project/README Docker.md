## Steps


- Run cmd/terminal in this directory

- run: `docker-compose up` command to see logs in the terminal (if the terminal is closed app will stop working!)

- run: `docker-compose up -d` command to start in detached mode (if the terminal is closed app will stay up)
- run: `docker-compose down` command to stop the app